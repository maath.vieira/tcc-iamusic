<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('propostas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->dateTime('dataEvento');
            $table->decimal('valor', 7,2);
            $table->integer('artista_id')->nullable()->onDelete('cascade');
            $table->foreign('artista_id')->references('id')->on('artistas')->onDelete('cascade');
            $table->integer('estabelecimento_id')->nullable()->onDelete('cascade');
            $table->foreign('estabelecimento_id')->references('id')->on('estabelecimentos')->onDelete('cascade');
            $table->integer('pendente')->default(0);
            $table->integer('aceita')->default(1);
            $table->integer('recusada')->default(1);
            $table->timestamps();
        });
**/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propostas');
    }
}
