<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('artistas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeArtistico', 50)->nullable();
            $table->string('cpf', 11);
            $table->string('integrantes', 1000)->nullable();
            $table->integer('usuario_id')->nullable()->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
**/
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artistas');
    }
}
