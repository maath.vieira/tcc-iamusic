<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('publicacoes', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('descricao');
            $table->integer('usuario_id')->nullable()->onDelete('cascade');
            $table->timestamps();
            $table->foreign('usuario_id')->references('user')->on('aux_artista_estabele')->onDelete('cascade');
            $table->integer('isAtivo')->default(0);
            $table->rememberToken();
        });
**/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicacoes');
    }
}
