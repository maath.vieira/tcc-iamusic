<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('agendas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->dateTime('data');
            $table->integer('artista_id')->nullable()->onDelete('cascade');
            $table->foreign('artista_id')->references('id')->on('artistas')->onDelete('cascade');
            $table->integer('estabelecimento_id')->nullable()->onDelete('cascade');
            $table->foreign('estabelecimento_id')->references('id')->on('estabelecimentos')->onDelete('cascade');
            $table->integer('isAtivo')->Default(0);
            $table->timestamps();
        });
**/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
