<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('comentarios', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('artista_id')->nullable()->unsigned()->onDelete('cascade');
            $table->integer('estabelecimento_id')->nullable()->unsigned()->onDelete('cascade');
            $table->timestamps();
        });
**/
/**
        Schema::table('comentarios', function (Blueprint $table) {
            $table->foreign('artista_id')->references('artista_id')->on('aux_comentario')->onDelete('cascade');
            $table->foreign('estabelecimento_id')->references('estabelecimento_id')->on('aux_comentario')->onDelete('cascade');
        });
**/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
