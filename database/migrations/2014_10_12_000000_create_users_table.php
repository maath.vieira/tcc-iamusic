<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/**
        Schema::create('users', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('nomeCompleto', 45);
            $table->string('email', 80)->unique();
            $table->string('senha', 45);
            $table->string('facebook', 200);
            $table->string('twitter', 200);
            $table->string('instagram', 200);
            $table->integer('cidade_id')->nullable();
            $table->foreign('cidade_id')->references('id')->on('cidades');
            $table->integer('tipoUsuario_id')->nullable();
            $table->foreign('tipoUsuario_id')->references('id')->on('tipousuarios');
            $table->integer('artista_id')->nullable()->onDelete('cascade');
            //$table->foreign('artista_id')->references('id')->on('artistas')->onDelete('cascade');
            $table->integer('estabelecimento_id')->nullable()->onDelete('cascade');
            $table->integer('isAtivo');
            //$table->foreign('estabelecimento_id')->references('id')->on('estabelecimentos')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();

        });

**/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
