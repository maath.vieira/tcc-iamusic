<?php

/**
$servername = "localhost";
$username = "u900193119_footd";
$password = "c8dshIBNadVu";
$dbname = "u900193119_dream";
*/
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "musicdb";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//$sql = "SELECT nomeCompleto FROM users";

$sql = "SELECT ag.id, ag.data, ar.nomeArtistico, es.razaoSocial, es.logradouro, es.bairro, ci.cidade, ci.uf 
		FROM agendas ag
		LEFT JOIN artistas ar ON ag.artista_id = ar.id
		LEFT JOIN estabelecimentos es ON ag.estabelecimento_id = es.id
		LEFT JOIN users us ON ar.usuario_id = us.id
		LEFT JOIN users us2 ON es.usuario_id = us2.id
		LEFT JOIN cidades ci ON us.cidade_id = ci.id
		WHERE ag.data BETWEEN CURRENT_DATE and adddate(CURRENT_DATE,+7)
		ORDER BY data";

$result = $conn->query($sql);

$lista = array();

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
            $lista[] = array("id" => utf8_encode($row["id"]),
							 "data" => utf8_encode(date("d/m/Y H:i", strtotime($row["data"]))),
							 "nomeArtistico" => utf8_encode($row["nomeArtistico"]),
							 "razaoSocial" => utf8_encode($row["razaoSocial"]),
							 "logradouro" => utf8_encode($row["logradouro"]),
							 "bairro" => utf8_encode($row["bairro"]),
							 "cidade" => utf8_encode($row["cidade"]),
							 "uf" => utf8_encode($row["uf"]));
    }
}

$conn->close();

header('Content-type: text/javascript');
echo json_encode($lista);
