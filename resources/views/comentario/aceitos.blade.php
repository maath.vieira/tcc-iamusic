@extends('layouts/artista')

@section('conteudo')

    @foreach($comentario as $com)
        <div class="container">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <section class="post-heading">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="media">
                                        <div class="media-left">
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading" style="color: #0e90d2;">{{$com->razaoSocial}} &rarr; {{$com->nomeArtistico}}</h4>
                                            <a href="#" class="anchor-time">{{date('d/m/Y h:i', strtotime($com->created_at))}}</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>

                        <section class="post-body">
                            <p>{{$com->comentario}}</p>
                        </section>

                        <a href="{{route('retirarComentario', $com->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Retirar Comentário</button>
                        </a>
                        </section>

                    </div>

                </div>
            </div>

        </div>
    @endforeach
@endsection

