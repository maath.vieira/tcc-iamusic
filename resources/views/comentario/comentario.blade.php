@extends('layouts/artista')

@section('conteudo')

<h2 style="margin-left:15px;">Comentários</h2>
<div class="col-sm-12">
      <form action="{{route('inserirCom')}}" method="post" >
        {{ csrf_field() }}
        <div class="form-group col-sm-12">
            <label for="artista">Selecione o Estabelecimento:</label>
            <select class="form-control" id="estabelecimento_id" name="estabelecimento_id">
              <option></option>
              @foreach($estabe as $e)
              <option value="{{$e->id}}">{{$e->razaoSocial}}</option>
              @endforeach
            </select>
        </div>
            <div class="form-group">
                      <div class="col-sm-12">
                          <textarea class="form-control" name="comentario" id="comentario" rows="5"style="resize: none;" ></textarea>
                      </div>
            </div>
            <input type="hidden" name="artista_id" id="artista_id" value="{{Auth::user()->artista_id}}">
            <input type="hidden" id="isAtivo" name="isAtivo" value="0">
            <input type="hidden" id="flg_enviada" name="flg_enviada" value="1">
            <input type="hidden" id="status" name="status" value="1">
            <div class="form-group" style="float: right; margin-top:5px;">
              <div class="col-sm-12">
                  <button class="btn btn-success btn-circle text-uppercase" type="submit">Enviar</button>
              </div>
            </div>
      </form>
</div>

    @foreach($comentario as $com)
    <div class="container">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <section class="post-heading">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="media">
                                    <div class="media-left">
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading" style="color: #0e90d2;">{{$com->razaoSocial}} &rarr; {{$com->nomeArtistico}}</h4>
                                        <a href="#" class="anchor-time">{{date('d/m/Y h:i', strtotime($com->created_at))}}</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>

                    <section class="post-body">
                        <p>{{$com->comentario}}</p>
                    </section>

                    <a href="{{route('aceitarComentario', $com->id)}}">
                        <button type="button" class="btn btn-link" style="float: right;" >Aceitar Comentário</button>
                    </a>
                    </section>

                </div>

            </div>
        </div>

    </div>
    @endforeach
@endsection
