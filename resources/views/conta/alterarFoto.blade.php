@extends('layouts/artista')

@section('conteudo')
    <div class="container">
        <h2>Alterar Foto</h2>
        <form method="post" action="{{route('upload')}}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <input type="file" id="foto" name="foto" accept="image/*">

            <button type="submit" class="btn btn-warning" style="margin-top: 5px;">Salvar Foto</button>
        </form>
    </div>
@endsection
