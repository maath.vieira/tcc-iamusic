@extends('layouts/artista')

@section('conteudo')
@foreach($editUsuario as $u)
    <div class="container">
        <h2>Dados Cadastrados</h2>
        <form method="post" action="{{route('atualizaConta', $u->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
            <label>Nome Completo</label>
            <input type="text" style="height: 40px; width: 400px;" class="form-control" id="nomeCompleto"
                   name="nomeCompleto" value="{{$u->nomeCompleto}}">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="text" style="width: 300px; height: 40px;" class="form-control" id="email"
                   name="email" value="{{$u->email}}">
            </div>
                <div class="form-group">
                <label>Facebook</label>
                <input type="text" style="width: 200px; height: 40px;" class="form-control" id="facebook"
                       name="facebook" value="{{$u->facebook}}">
                </div>
            <div class="form-group">
                <label>Twitter</label>
                <input type="text" style="width: 200px; height: 40px;" class="form-control" id="twitter"
                       name="twitter" value="{{$u->twitter}}" >
            </div>
            <div class="form-group">
                <label>Instagram</label>
                    <input type="text" style="width: 200px; height: 40px;" class="form-control" id="instagram"
                           name="instagram" value="{{$u->instagram}}">
            </div>

                <div class="form-group">
                    <label>Cidade</label>
                    <select class="form-control" style="width: 200px; height: 40px;" id="cidade_id" name="cidade_id">
                        @foreach($cidade as $c)
                            <option value="{{$c->id}}">{{$c->cidade}}</option>
                        @endforeach
                    </select>
                </div>

            <button type="submit" class="btn btn-warning">Salvar</button>
        </form>


    </div>

@endforeach
@endsection
