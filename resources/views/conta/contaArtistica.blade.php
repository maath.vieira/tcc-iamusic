@extends('layouts/artista')

@section('conteudo')
@foreach($artista as $u)
    <div class="container">
        <h2>Dados Cadastrados</h2>
        <form method="post" action="{{route('atualizaArtista', $u->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
            <label>Nome Completo</label>
            <input type="text" style="height: 40px; width: 400px;" class="form-control" id="nomeArtistico"
                   name="nomeArtistico" value="{{$u->nomeArtistico}}">
            </div>
            <div class="form-group">
            <label>CPF</label>
            <input type="text" style="width: 300px; height: 40px;" class="form-control" id="cpf"
                   name="cpf" value="{{$u->cpf}}" disabled="disabled">
            </div>
                <div class="form-group">
                <label>Integrantes</label>
                <input type="text" style="width: 200px; height: 40px;" class="form-control" id="integrantes"
                       name="integrantes" value="{{$u->integrantes}}">
                </div>

                <button type="submit" class="btn btn-success">Salvar</button>
        </form>
    </div>

@endforeach
@endsection
