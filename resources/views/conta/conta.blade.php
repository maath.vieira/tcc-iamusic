@extends('layouts/artista')

@section('conteudo')
@foreach($usuario as $u)
    <div class="container">
        <h2>Dados Cadastrados</h2>
        <form action="/action_page.php">
            <div class="form-group">
            <label>Nome Completo</label>
            <input type="text" style="height: 40px; width: 400px;" class="form-control" id="nomeCompleto"
                   name="nomeCompleto" value="{{$u->nomeCompleto}}" disabled="disabled">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="text" style="width: 300px; height: 40px;" class="form-control" id="email"
                   name="email" value="{{$u->email}}" disabled="disabled">
            </div>
                <div class="form-group">
                <label>Facebook</label>
                <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                       name="email" value="{{$u->facebook}}" disabled="disabled">
                </div>
            <div class="form-group">
                <label>Twitter</label>
                <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                       name="email" value="{{$u->twitter}}" disabled="disabled">
            </div>
            <div class="form-group">
                <label>Instagram</label>
                    <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                           name="email" value="{{$u->instagram}}" disabled="disabled">
            </div>
                <div class="form-group">
                    <label>Cidade</label>
                    <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                           name="email" value="{{$u->cidade}}" disabled="disabled">
                </div>
                <div class="form-group">
                    <label>Tipo</label>
                    <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                           name="email" value="{{$u->tipo}}" disabled="disabled">
                </div>
                <div class="form-group">
                    <label>Nome Artistico</label>
                    <input type="text" style="width: 200px; height: 40px;" class="form-control" id="email"
                           name="email" value="{{$u->nomeArtistico}}" disabled="disabled">
                </div>
            <a href="{{route('editConta', $u->id)}}">
                <button type="button" class="btn btn-warning">Editar</button>
            </a>

            <a href="{{route('editSenha', $u->id)}}">
                <button type="button" class="btn btn-link" style="float: right;" >Alterar Senha</button>
            </a>

            <a href="{{route('alterarFoto')}}">
                <button type="button" class="btn btn-link" style="float: right;" >Alterar Foto</button>
            </a>
        </form>
    </div>

@endforeach
@endsection
