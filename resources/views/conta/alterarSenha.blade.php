@extends('layouts/artista')

@section('conteudo')
    <div class="container">
        <h2>Alterar Senha</h2>
        <form method="post" action="{{route('atualizaSenha', $u->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <div class="form-group">
                    <label>Senha</label>
                    <input type="password" style="width: 200px; height: 40px;" class="form-control" id="senha"
                           name="senha" placeholder="Senha">
                </div>
            </div>
            <button type="submit" class="btn btn-warning">Salvar</button>
        </form>
    </div>
@endsection
