@extends('layouts/artista')

@section('conteudo')

    <div class="container">
        <h2>Propostas Aceitas</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Data</th>
                <th>Artista</th>
                <th>Estabelecimento</th>
                <th>CNPJ</th>
                <th>Valor</th>
            </tr>
            </thead>
            @foreach($listar as $prop)
                <tbody>
                <tr>
                    <td>{{date('d/m/Y', strtotime($prop->dataEvento))}}</td>
                    <td>{{$prop->nomeArtistico}}</td>
                    <td>{{$prop->razaoSocial}}</td>
                    <td>{{$prop->cnpj}}</td>
                    <td>{{$prop->valor}}</td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>

@endsection
