@extends('layouts/artista')

@section('conteudo')

    <h2>Propostas</h2>
    @foreach($proposta as $prop)
        <div class="activity-feed">
            <div class="feed-item">
                <div class="date">{{date('d/m/Y', strtotime($prop->dataEvento))}}</div>
                <div class="text">{{$prop->razaoSocial}} &rarr; {{$prop->nomeArtistico}}</div>
                <div class="text">R$ {{$prop->valor}}</div>
                <div>
                    <a href="{{route('aceitarProposta', $prop->id)}}">
                        <button type="button" id="id" name="id" class="btn btn-success" value="{{Auth::user()->artista_id}}">Aceitar</button>
                    </a>

                    <a href="{{route('recusarProposta', $prop->id)}}">
                        <button type="button" id="id" name="id" class="btn btn-danger" value="{{Auth::user()->artista_id}}">Recusar</button>
                    </a>
                </div>
            </div>
        </div>
    @endforeach

@endsection
