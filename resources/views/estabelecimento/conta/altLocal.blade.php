@extends('layouts/estabelecimento')

@section('conteudo')

@foreach($local as $l)
<div class="rover-primary-col rover-single-col col-center
    col-md-12 margin-top-x5">
    <div class="rover-primary-content-block">
        <header class="rover-page-header">
             <h1 class="rover-header-title"></h1>
             <h3 class="rover-header-subtitle"></h3>
        </header>
        <div class="new-design js-alerts-container"></div>
        <form method="post" action="{{route('atualizaEstaLocal')}}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
            <div class="account-profile-section-header margin-bottom-x5">
                 <h2>Conta</h2>
            </div>
                <div class="col-lg-12">
                        <div class="col-md-12">
                            <div id="div_id_account_information-address_line1" class="form-group">
                                <label for="id_account_information-address_line1" class="form-control-label  requiredField">Razão Social<span class="asteriskField"></span>
                                </label>
                                <div class="controls ">
                                    <input class="textinput textInput form-control" id="razaoSocial"
                                    maxlength="300" name="razaoSocial" required="required"
                                    type="text" value="{{$l->razaoSocial}}"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="div_id_account_information-address_line2" class="form-group">
                                <label for="id_account_information-address_line2" class="form-control-label ">Proprietário</label>
                                <div class="controls ">
                                    <input class="textinput textInput form-control" id="nomeProprietario"
                                    maxlength="150" name="nomeProprietario" type="text" value="{{$l->nomeProprietario}}" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_id_account_information-city" class="form-group">
                                <label for="id_account_information-city" class="form-control-label  requiredField">CNPJ<span class="asteriskField"></span>
                                </label>
                                <div class="controls ">
                                    <input class="textinput textInput form-control" id="cnpj"
                                    maxlength="75" name="cnpj" value="{{$l->cnpj}}" required="required" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_id_account_information-city" class="form-group">
                                <label for="id_account_information-city" class="form-control-label  requiredField">Rua<span class="asteriskField"></span>
                                </label>
                                <div class="controls ">
                                    <input class="textinput textInput form-control" id="logradouro"
                                    maxlength="175" name="logradouro" value="{{$l->logradouro}}" required="required" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_id_account_information-city" class="form-group">
                                <label for="id_account_information-city" class="form-control-label  requiredField">Bairro<span class="asteriskField"></span>
                                </label>
                                <div class="controls ">
                                    <input class="textinput textInput form-control" id="bairro"
                                    maxlength="75" name="bairro" value="{{$l->bairro}}" required="required" type="text"/>
                                </div>
                            </div>
                        </div>
                </div>
            <div class="padding-top-x5">
                <div class="padding-top-x5">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach
@endsection
