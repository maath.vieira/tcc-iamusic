@extends('layouts/estabelecimento')

@section('conteudo')
@foreach($editUsuario as $u)
<div class="padding-top-x5">
    <div class="padding-top-x5">
        <div style="float:right;">
          <a href="{{route('editarSenha')}}">
            <button type="submit" class="btn btn-warning">Alterar Senha da Conta</button>
          </a>
          <a href="{{route('editarLocal')}}">
            <button type="submit" class="btn btn-info">Configurações do Estabelecimento</button>
          </a>
        </div>
    </div>
</div>
       <div class="rover-primary-col rover-single-col col-center
           col-md-12 margin-top-x5">
           <div class="rover-primary-content-block">
               <header class="rover-page-header">
                    <h1 class="rover-header-title"></h1>
                    <h3 class="rover-header-subtitle"></h3>
               </header>
               <div class="new-design js-alerts-container"></div>
               <form method="post" action="{{route('atualizaEstaConta', $u->id)}}">
                 {{ csrf_field() }}
                 {{ method_field('PUT') }}
                   <div class="account-profile-section-header margin-bottom-x5">
                        <h2>Conta</h2>
                   </div>
                       <div class="col-lg-12">
                               <div class="col-md-12">
                                   <div id="div_id_account_information-address_line1" class="form-group">
                                       <label for="id_account_information-address_line1" class="form-control-label  requiredField">Nome Completo<span class="asteriskField"></span>
                                       </label>
                                       <div class="controls ">
                                           <input class="textinput textInput form-control" id="nomeCompleto"
                                           maxlength="50" name="nomeCompleto" required="required"
                                           type="text" value="{{$u->nomeCompleto}}"/>
                                       </div>
                                   </div>
                               </div>

                               <div class="col-md-12">
                                   <div id="div_id_account_information-address_line2" class="form-group">
                                       <label for="id_account_information-address_line2" class="form-control-label ">Email</label>
                                       <div class="controls ">
                                           <input class="textinput textInput form-control" id="email"
                                           maxlength="50" name="email" type="text" value="{{$u->email}}" required="required"/>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div id="div_id_account_information-city" class="form-group">
                                       <label for="id_account_information-city" class="form-control-label  requiredField">Facebook<span class="asteriskField"></span>
                                       </label>
                                       <div class="controls ">
                                           <input class="textinput textInput form-control" id="facebook"
                                           maxlength="75" name="facebook" value="{{$u->facebook}}" required="required" type="text"/>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div id="div_id_account_information-city" class="form-group">
                                       <label for="id_account_information-city" class="form-control-label  requiredField">Twitter<span class="asteriskField"></span>
                                       </label>
                                       <div class="controls ">
                                           <input class="textinput textInput form-control" id="twitter"
                                           maxlength="75" name="twitter" value="{{$u->twitter}}" required="required" type="text"/>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div id="div_id_account_information-city" class="form-group">
                                       <label for="id_account_information-city" class="form-control-label  requiredField">Instagram<span class="asteriskField"></span>
                                       </label>
                                       <div class="controls ">
                                           <input class="textinput textInput form-control" id="instagram"
                                           maxlength="75" name="instagram" value="{{$u->instagram}}" required="required" type="text"/>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <label>Cidade</label>
                                   <select class="form-control" id="cidade_id" name="cidade_id">
                                       @foreach($cidade as $c)
                                           <option value="{{$c->id}}">{{$c->cidade}}</option>
                                       @endforeach
                                   </select>
                               </div>
                       </div>
                   <div class="padding-top-x5">
                       <div class="padding-top-x5">
                           <div class="text-center">
                               <button type="submit" class="btn btn-success">Salvar</button>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
       </div>
       @endforeach
   </div>


@endsection
