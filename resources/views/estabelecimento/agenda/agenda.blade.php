@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/agenda/agenda.css" />

<form class="form-inline" method="post" action="{{route('salvarEsta')}}">
    {{ csrf_field() }}
    <input type="datetime-local" class="form-control" id="data" name="data">
    <select class="form-control" id="artista_id" name="artista_id">
    @foreach($artista as $art)
        <option value="{{$art->id}}">{{$art->nomeArtistico}}</option>
    @endforeach
    <input type="hidden" name="estabelecimento_id" id="estabelecimento_id" value="{{Auth::user()->estabelecimento_id}}">
    <input type="hidden" id="isAtivo" name="isAtivo" value="0">
    <button type="submit" class="btn btn-info">Adicionar</button>
</form>

<h2>Agenda</h2>
    <hr />

    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Hora</th>
                        <th>Evento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($evento as $event)
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1">
                            <div class="dayofmonth">{{date('d', strtotime($event->data))}}</div>
                            <div class="shortdate text-muted">/{{date('m/Y', strtotime($event->data))}}</div>
                        </td>
                        <td class="agenda-time">
                            {{date('H:i', strtotime($event->data))}}
                        </td>
                        <td class="agenda-events">
                            <div class="agenda-event">
                                {{$event->nomeArtistico}}
                            </div>
                        </td>
                        <td>
                          <a href="{{route('exAgenda', $event->id)}}">
                              <button type="button" class="btn btn-danger" name="id" id="id">Excluir</button>
                          </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


  @endsection
