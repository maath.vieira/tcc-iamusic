@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/publicacao/publicacao.css" />

    <h2>Editar Publicação</h2>
@foreach($publicacao as $p)
<form class="form-horizontal" method="POST" action="{{route('atualizarEstaPostagem', $p->id)}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-group">
        <label for="comment">Editar Publicação</label>
        <textarea class="form-control" rows="4" id="descricao" name="descricao" style="resize: none;" maxlength="5000">{{$p->descricao}}</textarea>
    </div>
    <div class="form-group">
        <button type="subimit" class="btn btn-info" style="float:right;">Salvar</button>
    </div>

</form>
@endforeach
@endsection
