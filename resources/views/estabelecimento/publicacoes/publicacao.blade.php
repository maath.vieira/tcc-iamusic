@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/publicacao/publicacao.css" />

    <h2>Publicações</h2>

    <form method="post" action="{{route('uploadEsta')}}" enctype="multipart/form-data">
      {{ csrf_field() }}
        <div class="col-sm-6">
          <input type="file" name="foto" id="foto" accept="image/*">
          <button type="submit" class="btn btn-info" style="margin-top:5px;">Publicar Foto</button>
        </div>
    </form>

    <form class="form-horizontal" method="POST" action="{{route('salvarPublicacao')}}">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="form-group col-sm-12">
            <div class="col-sm-6">
              <input type="text" class="form-control" id="video" name="video" placeholder="https://www.youtube.com/watch?v=PHgc8Q6qTjc">
            </div>

          </div>
            <textarea class="form-control" rows="4" id="descricao" name="descricao" style="resize: none;" maxlength="5000"></textarea>
        </div>
        <input type="hidden" id="usuario_id" name="usuario_id" value="{{Auth::user()->id}}">
        <input type="hidden" id="estabelecimento_id" name="estabelecimento_id" value="{{Auth::user()->estabelecimento_id}}">
        <div class="form-group">
            <button type="subimit" class="btn btn-info" style="float:right;">Publicar</button>
        </div>
    </form>

@foreach($postagem as $post)
   <div class="row col-md-12">
      <div class="col-md-10 col-lg-10">
         <div ></div>
         <div id="tracking">
            <div class="tracking-list">
               <div class="tracking-item">
                  <div class="tracking-icon status-intransit">
                     <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                     </svg>
                     <!-- <i class="fas fa-circle"></i> -->
                  </div>
                  <div class="tracking-name">{{Auth::user()->nomeCompleto}}</div>
                  <div class="tracking-date" style="font-size: 10px;">{{date('d/m/Y', strtotime($post->created_at))}}</div>
                  <div class="tracking-content" style="font-size: 15px;">
                    @if(is_null($post->descricao) && is_null($post->foto))
                      <!--<iframe width="560" height="315" src="{{$post->video}}"></iframe>-->
                      <iframe width="660" height="360" src="{{$post->video}}"></iframe>
                      <div>
                        <a href="{{route('excluirPost', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Excluir Postagem</button>
                        </a>
                      </div>
                    @elseif(is_null($post->descricao) && is_null($post->video))
                      <img src="/../storage/{{$post->foto}}.jpg" alt="..." >
                      <div>
                        <a href="{{route('excluirPosta', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Excluir Postagem</button>
                        </a>
                      </div>
                    @else
                        {{$post->descricao}}
                        <div>
                        <a href="{{route('excluirPost', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Excluir Postagem</button>
                        </a>
                        <a href="{{route('editarPosta', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right; margin-right: 5px;" >Editar Postagem</button>
                        </a>
                      </div>
                    @endif
                  </div>
               </div>
         </div>
      </div>
   </div>
</div>
@endforeach

@endsection
