@extends('layouts/estabelecimento')

@section('conteudo')

<div class="rover-primary-col rover-single-col col-center
    col-md-12 margin-top-x5">
    <div class="rover-primary-content-block">
        <header class="rover-page-header">
             <h1 class="rover-header-title"></h1>
             <h3 class="rover-header-subtitle"></h3>
        </header>
        <div class="new-design js-alerts-container"></div>
        @foreach($ranki as $r)
        <form method="post" action="{{route('insereRankingE', $r->id)}}">
          {{ csrf_field() }}
            <div class="account-profile-section-header margin-bottom-x5">
                 <h2>Perguntas</h2>
                 <p>Antes de continuar responda as perguntas abaixo, por favor.</p>
            </div>
                <div class="col-lg-12">

                        <div class="col-md-10">
                            <label>Qual a nota que você da para a banda?</label>
                            <select class="form-control" id="nota_um" name="nota_um">
                                    <option value="1">Péssimo</option>
                                    <option value="2">Ruim</option>
                                    <option value="3">Razoável</option>
                                    <option value="4">Bom</option>
                                    <option value="5">Ótimo</option>
                            </select>
                        </div>
                        <div class="col-md-10" style="margin-top:5px;">
                            <label>Cidade</label>
                            <select class="form-control" id="nota_dois" name="nota_dois">
                              <option value="1">Péssimo</option>
                              <option value="2">Ruim</option>
                              <option value="3">Razoável</option>
                              <option value="4">Bom</option>
                              <option value="5">Ótimo</option>
                            </select>
                        </div>
                        <div class="col-md-10" style="margin-top:5px;">
                            <label>Cidade</label>
                            <select class="form-control" id="nota_tres" name="nota_tres">
                              <option value="1">Péssimo</option>
                              <option value="2">Ruim</option>
                              <option value="3">Razoável</option>
                              <option value="4">Bom</option>
                              <option value="5">Ótimo</option>
                            </select>
                        </div>
                </div>

            <div class="padding-top-x5">
                <div class="padding-top-x5">
                    <div class="col-sm-10 text-right">
                        <button type="submit" style="margin-top:5px;" class="btn btn-success">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
        @endforeach
    </div>
</div>

@endsection
