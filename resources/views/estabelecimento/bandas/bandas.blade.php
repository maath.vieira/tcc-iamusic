@extends('layouts/estabelecimento')

@section('conteudo')

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<form class="form-inline" method="get" action="{{route('perfilBanda')}}">
  {{ csrf_field() }}
  <input type="datetime-local" class="form-control" id="dataPesquisa" name="dataPesquisa" style="height:30px;">
  <button type="submit" class="btn btn-primary">Verificar</button>
</form>

<div class="row">
        <div class="span10">
    		<ul class="thumbnails">
          @foreach ($banda as $b)
                <li class="span5 clearfix">
                  <div class="thumbnail clearfix">
                    <img src="/storage/img/perfil/{{$b->usuario_id}}.jpg" alt="ALT NAME" class="pull-left span2 clearfix" style="margin-right:10px; Width:100px; height:100px;">
                    <div class="caption" class="pull-left">
                      <form class="" method="post" action="{{route('perfilB', $b->id)}}">
                        {{ csrf_field() }}
                        <!--<input type="hidden" id="artista_id" name="artista_id" value="{{$b->id}}"/>-->
                        <button type="submit" class="btn btn-primary icon  pull-right">Perfil</button>
                      </form>
                      <h4>
                      <a href="#" >{{$b->nomeArtistico}}</a>
                      </h4>
                      <small><b>Localidade: </b>{{$b->cidade}},{{$b->uf}}</small>
                    </div>
                  </div>
                </li>
            @endforeach
            </ul>
        </div>
	</div>


@endsection
