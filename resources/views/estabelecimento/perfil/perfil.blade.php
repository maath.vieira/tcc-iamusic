@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/perfil/perfil.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

@foreach ($perfilBanda as $pf)
<div class="row">
      <div class="col-md-12 text-center ">
        <div class="panel panel-default">
          <div class="userprofile social ">
            <div class="userpic"> <img src="/../storage/img/perfil/{{$pf->usuario_id}}.jpg" alt="" class="userpicimg"> </div>
            <h3 class="username">{{$pf->nomeArtistico}}</h3>
            <p>{{$pf->cidade}}, {{$pf->uf}}</p>
            <div class="socials tex-center"> <a href="https://www.facebook.com/{{$pf->facebook}}" class="btn btn-circle btn-primary "><i class="fa fa-facebook"></i></a>
			<a href="https://www.instagram.com/{{$pf->instagram}}" class="btn btn-circle btn-danger "><i class="fa fa-instagram"></i></a>
			<a href="https://twitter.com/{{$pf->twitter}}" class="btn btn-circle btn-info "><i class="fa fa-twitter"></i></a>
            </div>
          </div>
          <div class="col-md-12 border-top border-bottom">
            <ul class="nav nav-pills pull-left countlist" role="tablist">
              <li role="presentation">
                <h3>1452º<br>
                  <small>Ranking</small> </h3>
              </li>

              <a href="{{('/perfilProposta')}}"><button type="button" style="margin-top: 15px;" class="btn btn-success">Enviar Proposta</button></h3></a>
              <a href="{{('/perfilBanda')}}"><button type="button" style="margin-top: 15px;" class="btn btn-warning">Voltar</button></a></h3>
              <a href="{{('/perfilBanda')}}"><button type="button" style="margin-top: 15px;" class="btn btn-info">Enviar Comentário</button></a></h3>
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- /.col-md-12 -->
      <div class="col-md-4 col-sm-12 pull-right">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="page-header small">Detalhes</h1>
          </div>
          <div class="col-md-12 photolist">
            <div class="row">
              <div style="margin-left:5px;"><p>Integrantes: {{$pf->integrantes}}</p></div>
              <div style="margin-left:5px;"><p>Ranking: </p></div>
              <div style="margin-left:5px;"><p>Facebook: {{$pf->facebook}}</p></div>
              <div style="margin-left:5px;"><p>Instagram: {{$pf->instagram}}</p></div>
			  <div style="margin-left:5px;"><p>Twitter: {{$pf->twitter}}</p></div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="page-header small">Agenda</h1>
            <p class="page-subtitle small">São listados apenas os próximos 5 enventos.</p>
          </div>
          <div class="col-md-12">
            <ul class="list-group">
              @foreach ($agendaBanda as $ab)
              <li class="list-group-item"> {{date('d/m/Y', strtotime($ab->data))}} - {{$ab->razaoSocial}}</li>
            @endforeach
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="page-header large">Comentários</h1>
            <p class="page-subtitle small">São listados apenas os 5 últimos Comentários.</p>
          </div>
          <div class="col-md-12">
            <ul class="list-group">
              @foreach ($comentarios as $c)
              <li class="list-group-item"> <p>{{$c->comentario}}</p></li>
            @endforeach
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      @foreach ($publiBanda as $pb)
      <div class="col-md-8 col-sm-12 pull-left posttimeline">
        <div class="panel panel-default">
          <div class="btn-group pull-right postbtn">

          </div>
          <div class="col-md-12" style="margin-top: 10px;">
            <div class="media">
              <div class="media-left"> <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="" class="media-object"> </a> </div>
              <div class="media-body">
                <h4 class="media-heading">{{$pb->nomeArtistico}}<br>
                  <small><i class="fa fa-clock-o"></i> {{date('d/m/Y H:i', strtotime($pb->created_at))}}</small> </h4>
                  <p>
                    @if(is_null($pb->descricao) && $pb->video)
                      <iframe width="560" height="315" src="{{$pb->video}}"></iframe>
                    @elseif($pb->descricao)
                        {{$pb->descricao}}
                    @else
                      <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url={{$pb->sound}}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                    @endif
              </p>

                <ul class="nav nav-pills pull-left ">
                  <li><a href="" title=""><i class="glyphicon glyphicon-share-alt" ></i> Compartilhe</a></li>
                </ul>
              </div>
            </div>
          </div>

        </div>

      </div>
      @endforeach
    </div>
</div>
@endforeach

@endsection
