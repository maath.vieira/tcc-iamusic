@extends('layouts/estabelecimento')

@section('conteudo')

<h1>Proposta</h1>
<form method="post" action="{{route('enviarProposta')}}">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="data">Data do evento:</label>
    <input type="datetime-local" class="form-control" id="dataEvento" name="dataEvento" required>
  </div>
  <div class="form-group">
      <label for="artista">Selecione o Artista:</label>
      <select class="form-control" id="artista_id" name="artista_id">
        <option></option>
        @foreach($artistas as $a)
        <option value="{{$a->id}}">{{$a->nomeArtistico}}</option>
        @endforeach
      </select>
  </div>
  <div class="form-group">
    <label for="Email">Email para contato</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
  </div>
  <div class="form-group">
    <label for="tel">Telefone</label>
    <input type="text" class="form-control" id="tel" name="tel" placeholder="(00)000000000" required>
  </div>
  <div class="form-group">
    <label for="Whatsapp">Whatsapp</label>
    <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="(00)9000000000" required>
  </div>
  <div class="form-group">
    <label for="valor">Valor da proposta em R$</label>
    <input type="number" class="form-control" id="valor" name="valor" placeholder="1500,00" required>
  </div>
  <input type="hidden" name="estabelecimento_id" id="estabelecimento_id" value="{{Auth::user()->estabelecimento_id}}">
  <div class="row" style="float: right">
  <div class="col-sm-12">
    <button type="submit" class="btn btn-info" color: white;">Enviar Proposta</button>
  </div>
</div>
</form>

@endsection
