@extends('layouts/estabelecimento')

@section('conteudo')

    <h2>Lista de Propostas</h2>
    <hr>
    <div class="row">
        <div class="col-xs-12" style="">
            <div class="panel panel-default list-group-panel">
                <div class="panel-body">
                    <ul class="list-group list-group-header">
                        <li class="list-group-item list-group-body">
                            <div class="row">
                                <div class="col-xs-4 text-left">Artista</div>
                                <div class="col-xs-2">Valor</div>
                                <div class="col-xs-2">Data</div>
                                <div class="col-xs-2">Status</div>
                                <div class="col-xs-2">Ação</div>
                            </div>
                        </li>
                    </ul>

                    @foreach ($lista as $list)
                    <ul class="list-group list-group-body" style="">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-4 text-left" style=" "> <a><span aria-hidden="true"></span> {{$list->nomeArtistico}} </a> </div>
                                <div class="col-xs-2" style="">{{$list->valor}}</div>
                                <div class="col-xs-2" style="">{{date('d/m/Y', strtotime($list->dataEvento))}}</div>
                                <div class="col-xs-2" style="">@if($list->pendente === 0)
                                                                  <p style="color: orange;">Pendente</p>
                                                               @elseif ($list->aceita === 0)
                                                                  <p style="color: green;">Aceita</p>
                                                              @elseif ($list->recusada === 0)
                                                                  <p style="color: red;">Recusada</p>
                                                              @endif
                              </div>
                              <div class="col-xs-2" style="">@if($list->pendente === 0)
                                                              <a href="{{route('excluirProposta', $list->id )}}">
                                                                <button type="button" class="btn btn-danger"  >Cancelar</button>
                                                              </a>
                                                              @endif
                              </div>
                            </div>
                        </li>
                    </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
