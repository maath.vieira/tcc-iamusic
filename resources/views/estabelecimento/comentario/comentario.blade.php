@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/comentario/style.css">

<div style="float: right;">
<p>
<a href="{{('ecomentarioAceitos')}}">
<button type="button" class="btn btn-warning">Aceitos</button>
</a>
<a href="{{('ecomentarioEnviados')}}">
<button type="button" class="btn btn-success">Enviados</button>
</a>
</p>
</div>

<h2 style="margin-left:15px;">Comentários</h2>
<div class="col-sm-12">
      <form action="{{route('inserirComentario')}}" method="post" >
        {{ csrf_field() }}
        <div class="form-group col-sm-12">
            <label for="artista">Selecione o Artista:</label>
            <select class="form-control" id="artista_id" name="artista_id">
              <option></option>
              @foreach($artistas as $a)
              <option value="{{$a->id}}">{{$a->nomeArtistico}}</option>
              @endforeach
            </select>
        </div>
            <div class="form-group">
                      <div class="col-sm-12">
                          <textarea class="form-control" name="comentario" id="comentario" rows="5"style="resize: none;" ></textarea>
                      </div>
            </div>
            <input type="hidden" name="estabelecimento_id" id="estabelecimento_id" value="{{Auth::user()->estabelecimento_id}}"
            <input type="hidden" id="isAtivo" name="isAtivo" value="0">
            <input type="hidden" id="flg_enviada" name="flg_enviada" value="2">
            <input type="hidden" id="status" name="status" value="1">
            <div class="form-group" style="float: right; margin-top:5px;">
              <div class="col-sm-12">
                  <button class="btn btn-success btn-circle text-uppercase" type="submit">Enviar</button>
              </div>
            </div>
      </form>
</div>
@foreach($comentario as $c)
        <div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-9 col-md-9 section-box">
                        <h2>
                            {{$c->nomeArtistico}}&rarr;{{$c->razaoSocial}}
                            </span>
                        </h2>
                        <hr />
                        <p>
                            {{$c->comentario}}
                            <a href="{{route('acComentario', $c->id)}}">
                                <button type="button" class="btn btn-default" style="float: right;">Aceitar Comentário</button>
                            </a>
                    </div>
                </div>
            </div>
        </div>
@endforeach

@endsection
