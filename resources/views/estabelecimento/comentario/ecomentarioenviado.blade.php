@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/comentario/style.css">

<div style="float: right;">
<p>
  <a href="{{('ecomentarioAceitos')}}">
  <button type="button" class="btn btn-warning">Aceitos</button>
  </a>
  <a href="{{('comentarioEstabelecimento')}}">
  <button type="button" class="btn btn-primary">Pendentes</button>
  </a>
</div>

@foreach($comentario as $c)
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-9 col-md-9 section-box">
                        <h2>
                            {{$c->razaoSocial}}&rarr;{{$c->nomeArtistico}}
                            </span>
                        </h2>
                        <hr />
                        <p>
                            {{$c->comentario}}
                            <a href="{{route('exComentario', $c->id)}}">
                                <button type="button" class="btn btn-default" style="float: right;">Excluir Comentário</button>
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

@endsection
