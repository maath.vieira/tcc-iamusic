@extends('layouts/estabelecimento')

@section('conteudo')

<link rel="stylesheet" href="/css/estabele/comentario/style.css">

<div style="float: right;">
<p>
<a href="{{('comentarioEstabelecimento')}}">
<button type="button" class="btn btn-warning">Pendentes</button>
</a>
<a href="{{('ecomentarioEnviados')}}">
<button type="button" class="btn btn-success">Enviados</button>
</a>
</div>

@foreach($comentario as $c)
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-9 col-md-9 section-box">
                        <h2>
                            {{$c->nomeArtistico}}&rarr;{{$c->razaoSocial}}
                            </span>
                        </h2>
                        <hr />
                        <p>
                            {{$c->comentario}}
                            <a href="{{route('reComentario', $c->id)}}">

                                <button type="button" class="btn btn-default" style="float: right;">Retirar Comentário</button>
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

@endsection
