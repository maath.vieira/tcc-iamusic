@extends('layouts/artista')

@section('conteudo')


        <form class="form-horizontal" method="POST" action="{{route('salvarPost')}}">
            {{ csrf_field() }}
            <div class="form-group col-sm-12">
              <div class="col-sm-6">
                <input type="text" class="form-control" id="video" name="video" placeholder="https://www.youtube.com/watch?v=PHgc8Q6qTjc">
              </div>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="sound" name="sound" placeholder="https://soundcloud.com/nlechoppa/capo">
              </div>
            </div>
            <div class="form-group">
                <label for="comment">Nova postagem</label>
                <textarea class="form-control" rows="4" id="descricao" name="descricao" style="resize: none;" maxlength="5000"></textarea>
            </div>
            <div class="col-sm-6">
              <input type="file" name="pic" accept="image/*">
            </div>
            <input type="hidden" id="usuario_id" name="usuario_id" value="{{Auth::user()->id}}">
            <input type="hidden" id="artista_id" name="artista_id" value="{{Auth::user()->artista_id}}">
            <div class="form-group">
                <button type="subimit" class="btn btn-info" style="float:right;">Publicar</button>
            </div>

        </form>

        <h2 id="timeline">Publicações</h2>

        @foreach($postagens as $post)
            <div class="row">
                <div class="col-sm-12">
                    <div class="item">
                        <span class="fa fa-check"></span>
                        <p style="float:right;">{{date('d/m/Y', strtotime($post->created_at))}}</p>

                            <div>{{$post->nomeArtistico}}</div>
                        <p>
                            @if(is_null($post->descricao) && $post->video)
                              <iframe width="560" height="315" src="{{$post->video}}"></iframe>
                            @elseif($post->descricao)
                                {{$post->descricao}}
                            @else
                              <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url={{$post->sound}}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                            @endif
                        </p>

                        <a href="{{route('excluirPost', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Excluir Postagem</button>
                        </a>
                        <a href="{{route('editarPostagem', $post->id)}}">
                            <button type="button" class="btn btn-link" style="float: right;" >Editar Postagem</button>
                        </a>
                    </div>
                    </div>
                </div>
        @endforeach

    </div>
@endsection
