@extends('layouts/artista')

@section('conteudo')
    @foreach($publi as $p)
    <div class="container">
        <form class="form-horizontal" method="post" action="{{route('atualizarPostagem', $p->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <label for="comment">Editar postagem</label>
                <textarea class="form-control" rows="4" id="descricao" name="descricao" style="resize: none;"
                          maxlength="5000">{{$p->descricao}}</textarea>
                    @endforeach
            </div>
            <div class="form-group">
                <button type="subimit" class="btn btn-info" style="float:right;" >Salvar</button>
            </div>

        </form>
    </div>
@endsection
