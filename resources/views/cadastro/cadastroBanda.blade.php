@extends('layouts.artista')

@section('conteudo')

    <form class="form-horizontal" method="post" action="{{route('salvarCadastroBanda')}}">

    {{ csrf_field() }}

            <h2 style="text-align: center;">Bem vindo, seu cadastro foi efetuado com sucesso!</h2>
            <p  style="text-align: center;">Antes de proseguir, necessitamos de mais algumas informações!</p>

            <div class="form-group" style="margin-top: 50px;">
                <label class="col-md-4 control-label" for="cod_user">Nome Artístico</label>
                <div class="col-md-4">
                    <input id="nomeArtistico" name="nomeArtistico" type="text" placeholder="Ex.: Zezinho das gaitas, Guns n' Roses Cover..." class="form-control input-md">
                    <p>Caso for banda coloque o nome da mesma no campo acima.</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="cpf">CPF</label>
                <div class="col-md-4">
                    <input id="cpf" name="cpf" type="text" placeholder="Ex.: 12345678976" maxlength="11" class="form-control input-md" required="">
                    <p>Sem caracteres especiais.</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="integrantes">Integrantes</label>
                <div class="col-md-4">
                    <input id="integrantes" name="integrantes" type="text" placeholder="Ex.: Matheus, Luiz, Roberto" class="form-control input-md" required="">
                    <p>Separe os nomes por virgula.</p>
                </div>
            </div>

            <input type="hidden" id="usuario_id" name="usuario_id" value="{{Auth::user()->id}}">


        <div class="form-group">

                <label class="col-md-4 control-label" for="pesquisar"></label>
                <div >
                    <button id="cadastrar" name="cadastrar" class="btn btn-primary">Cadastrar</button>
                </div>
            </div>

    </form>


@endsection
