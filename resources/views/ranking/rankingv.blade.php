@extends('layouts/artista')

@section('conteudo')

  <h2>Ranking Artista</h2>
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nome</th>
        <th>Nota</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ranki2 as $r)
      <tr>
        <td></td>
        <td>{{$r->nomeArtistico}}</td>
        <td>{{$r->nota}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <hr />

  <h2>Ranking Estabelecimento</h2>
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nome</th>
        <th>Nota</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ranki3 as $re)
      <tr>
        <td></td>
        <td>{{$re->razaoSocial}}</td>
        <td>{{$re->nota}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

@endsection
