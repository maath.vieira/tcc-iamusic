<!DOCTYPE html>
<html lang="en">
<head>

    <title>Iamusic - {{Auth::user()->nomeCompleto}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/css/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/css/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/css/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/css/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/css/custom.min.css" rel="stylesheet">
    <link href="/css/feedback.css" rel="stylesheet">
    <link href="/css/timeline.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title" style="text-align:center;"> <span><img src="/img/logo.png"></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="/storage/img/perfil/{{Auth::user()->id}}.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bem Vindo,</span>
                            <h2>{{Auth::user()->nomeCompleto}}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a href="{{('/')}}"><i class="fa fa-home"></i> Início </a>
                            </li>
                            <!--
                            <li><a><i class="fa fa-music"></i> Artista / Banda <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{('banda')}}">Visualizar</a></li>
                                </ul>
                            </li>
                          -->
                            <li><a><i class="fa fa-usd"></i> Propostas <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{('proposta')}}">Pendentes</a></li>
                                    <li><a href="{{('aceitas')}}">Aceitas</a></li>
                                    <li><a href="{{('recusadas')}}">Recusadas</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-calendar-check-o"></i> Agenda <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{('agenda')}}">Visualizar</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-comment"></i> Comentários <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{('comentario')}}">Pendentes</a></li>
                                    <li><a href="{{'aceitos'}}">Aceitos</a></li>
                                    <li><a href="{{'feitos'}}">Feitos</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-file"></i>Estabelecimentos <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                  <li><a href="{{('/perfilEstabelecimento')}}">Perfil</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-line-chart"></i>Ranking <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{'rankingv'}}">Visualizar Ranking</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-user"></i>Conta <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{'conta'}}">Minha Conta</a></li>
                                    <li><a href="{{'contaArtistica'}}">Conta Artista</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->


            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                 {{ Auth::user()->nomeCompleto }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Sair') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <!-- top tiles -->
            <div class="row tile_count">
                @yield('conteudo')
            </div>
            <!-- /top tiles -->

        </div>
    </div>

    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/js/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/js/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/js/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/js/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/js/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/js/skycons.js"></script>
    <!-- Flot -->
    <script src="/js/jquery.flot.js"></script>
    <script src="/js/jquery.flot.pie.js"></script>
    <script src="/js/jquery.flot.time.js"></script>
    <script src="/js/jquery.flot.stack.js"></script>
    <script src="/js/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/js/jquery.flot.orderBars.js"></script>
    <script src="/js/jquery.flot.spline.min.js"></script>
    <script src="/js/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/js/date.js"></script>
    <!-- JQVMap -->
    <script src="/js/jquery.vmap.js"></script>
    <script src="/js/jquery.vmap.world.js"></script>
    <script src="/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/js/moment.min.js"></script>
    <script src="/js/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/js/custom.min.js"></script>


</div>
</body>
</html>
