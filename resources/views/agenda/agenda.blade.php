@extends('layouts/artista')

@section('conteudo')

    <link href="/css/agenda.css" rel="stylesheet">

        <h3>Adicionar Eventos</h3>
        <form class="form-inline" method="post" action="{{route('salvarEvento')}}">
            {{ csrf_field() }}
            <input type="datetime-local" class="form-control" id="data" name="data">
            <select class="form-control" id="estabelecimento_id" name="estabelecimento_id">
                @foreach($local as $l)
                    <option></option>
                    <option value="{{$l->id}}">{{$l->razaoSocial}}</option>
                    @endforeach
            </select>
            <input type="hidden" name="artista_id" id="artista_id" value="{{Auth::user()->artista_id}}">
            <input type="hidden" id="isAtivo" name="isAtivo" value="0">
            <button type="submit" class="btn btn-primary" style="margin-top:5px;">Adicionar</button>
        </form>

        <h3>Minha Agenda</h3>
        <div class="container">
            @foreach($agenda as $a)
            <ul class="timeline">
                <li>
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Horário: {{date('d/m/Y H:i', strtotime($a->data))}}</h4>
                            <h4 class="timeline-title">Local: {{$a->razaoSocial}}</h4>
                            <h4 class="timeline-title">Você: {{$a->nomeArtistico}}</h4>
                            <h4 class="timeline-title">Endereço: {{$a->logradouro}}</h4>
                        </div>
                        <!--
                        <a href="{{route('editarEvento', $a->id)}}">
                            <button type="button" class="btn btn-warning">Editar</button>&nbsp;
                        </a>
                      -->
                        <a href="{{route('excluirEvento', $a->id)}}">
                            <button type="button" class="btn btn-danger" name="id" id="id">Excluir</button>
                        </a>
                    </div>
                </li>
            </ul>
            @endforeach
        </div>

    </div>

@endsection
