@extends('layouts/artista')

@section('conteudo')

    <div class="container">
@foreach($editar as $e)
        <h3>Adicionar Eventos</h3>
        <form class="form-inline" method="post" action="{{route('atualizarEvento', $e->id)}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            @foreach($local as $l)
            <h4>{{date('d/m/Y H:i', strtotime($e->data))}} &rarr; {{$l->razaoSocial}}</h4>
            @endforeach
            <input type="datetime-local" class="form-control" id="data" name="data" value="{{$e->data}}"/>
            <button type="submit" class="btn btn-primary">Atualizar</button>
        </form>
    </div>
@endforeach
@endsection
