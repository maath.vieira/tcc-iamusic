@extends('layouts/artista')

@section('conteudo')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/banda.css" rel="stylesheet">
    <link href="/css/icones.css" rel="stylesheet">

    <div class="container-fluid gedf-wrapper">
        <div class="row">

            <div class="col-md-6 gedf-main">
@foreach($bandas as $b)
    <div class="card gedf-card">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="mr-2">
                    </div>
                    <div class="ml-2">
                        <div class="h5 m-0">{{$b->nomeArtistico}}</div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body">
            <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>&nbsp;{{date('d/m/Y H:i', strtotime($b->created_at))}}</div>
            <p class="card-text">
                {{$b->descricao}}
            </p>
        </div>
        <div class="card-footer">
            <a href="#" class="card-link"><i class="fa fa-mail-forward"></i> Compartilhar</a>
        </div>
    </div>
  }
@endforeach
    </div>
    
    <div class="col-md-3">
        <div class="card gedf-card">
            <div class="card-body">
                <img src="/storage/img/perfil/{{Auth::user()->id}}.jpg" alt="..." style="max-width: 50px; max-width: 50px;"/>
                <h5 class="card-title">Usuário</h5>
                <h6 class="card-subtitle">{{Auth::user()->nomeCompleto}}</h6>
                <h5 class="card-title">Integrantes</h5>
                <h6 class="card-subtitle">{{$b->nomeArtistico}}</h6>
                <p class="card-text">{{$b->integrantes}}</p>
            </div>
        </div>
        <div class="card gedf-card">
            <div class="card-body">
                <h5 class="card-title">Redes Sociais</h5>
                <p class="card-text">
                    <a href="https://www.facebook.com/{{$b->facebook}}" target="_blank"><i class="fa fa-facebook-square" style="font-size:24px; margin-right:5px;"></i></a>
                    <a href="https://twitter.com/search?q={{$b->twitter}}&src=typd" target="_blank"><i class="fa fa-twitter-square"  style="font-size:24px; margin-right:5px;"></i></a>
                    <a href="https://www.instagram.com/{{$b->instagram}}/" target="_blank"><i class="fa fa-instagram"  style="font-size:24px; margin-right:5px;"></i></a>
                </p>
            </div>
        </div>
    </div>
    </div>
    </div>


@endsection
