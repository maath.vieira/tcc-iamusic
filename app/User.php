<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nomeCompleto', 'email', 'senha', 'facebook', 'twitter', 'instagram', 'cidade_id', 'tipoUsuario_id', 'artista_id', 'estabelecimento_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha', 'remember_token',
    ];


    public function getAuthPassword()
    {
        return $this->attributes['senha'];
    }


}
