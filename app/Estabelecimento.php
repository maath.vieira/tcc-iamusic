<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estabelecimento extends Model
{
    protected $fillable = [
        'razaoSocial', 'nomeProprietario', 'cnpj', 'logradouro', 'bairro', 'usuario_id'
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['estabelecimentos'];
}
