<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $fillable = [
        'data', 'artista_id', 'estabelecimento_id',
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['agendas'];
}
