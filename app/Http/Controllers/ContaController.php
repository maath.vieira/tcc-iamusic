<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ContaController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $usuario = DB::table('users')
            ->join('artistas', 'users.artista_id', '=', 'artistas.id')
            ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
            ->join('tipousuarios', 'users.tipoUsuario_id', '=', 'tipousuarios.id')
            ->select('users.*', 'artistas.nomeArtistico', 'cidades.cidade', 'tipousuarios.tipo')
            ->where('artista_id', Auth::user()->artista_id)
            ->get();

        return view('conta/conta', compact('usuario'));
    }

    public function edit($id){

        $editUsuario = DB::table('users')
            ->select('users.*')
            ->where('id', $id)
            ->get();


        $cidade = DB::table('cidades')
            ->select('cidades.id', 'cidades.cidade')
            ->get();

        return view('conta/editar', compact('editUsuario', 'cidade'));
    }

    public function update(Request $request, $id){

        $nomeCompleto = $request->nomeCompleto;
        $email = $request->email;
        $facebook = $request->facebook;
        $twitter = $request->twitter;
        $instagram = $request->instagram;
        $cidade = $request->cidade_id;


        $conta = DB::update("update users set nomeCompleto = '$nomeCompleto', email = '$email', facebook = '$facebook', twitter = '$twitter', instagram = '$instagram', cidade_id = $cidade where id = $id");

        return redirect()->route('conta');
    }

    public function editSenha($id){
        $editSenha = DB::table('users')
            ->select('users.*')
            ->where('id', $id)
            ->get();

        return view('conta/alterarSenha', compact('editSenha'));
    }

    public function updateSenha(Request $request, $id){
        $senha = Hash::make($request['senha']);

        $senha = DB::update("update users set senha = '$senha' where id = $id");

        return redirect()->route('conta');
    }

    public function indexArtista(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $artista = DB::table('artistas')
            ->join('users', 'artistas.id', '=', 'users.artista_id')
            ->select('artistas.*')
            ->where('artista_id', Auth::user()->artista_id)
            ->get();

        return view('conta/contaArtistica', compact('artista'));
    }

    public function updateArtista(Request $request, $id){

        $nomeArtistico = $request->nomeArtistico;
        $integrantes = $request->integrantes;

        $artista = DB::update("update artistas set nomeArtistico = '$nomeArtistico', integrantes = '$integrantes'  where id = $id");

        return redirect()->route('conta');
    }

    public function alterarFoto(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        return view('conta/alterarFoto');
      }

      public function upload(Request $request){
          $foto = $request->file('foto');
          $ext = ['jpg', 'png', 'jpeg'];
          if($foto->isValid() and in_array($foto->extension(), $ext)){
              $foto->storeAs('img/perfil', Auth::user()->id.'.jpg');
              return redirect()->route('inicio');
            }
    }

    public function publicaFotoEsta(Request $request){
        $foto = $request->file('foto');
        $ext = ['jpg', 'png', 'jpeg'];
        if($foto->isValid() and in_array($foto->extension(), $ext)){
            $foto->storeAs('img/publi', Auth::user()->id.'.jpg');
            $cliente = DB::insert("insert into aux_artista_estabele (user, estabelecimento_id) values (?,?)",
            [$request->usuario_id, $request->estabelecimento_id]);
            $postagem = DB::insert("insert into publicacoes (descricao, foto, usuario_id) values (?,?,?)",
            [$request->descricao, $foto, $request->usuario_id]);
            return redirect()->route('realPublicacao');
        }
    }

    public function estaConta(){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $editUsuario = DB::table('users')
          ->select('users.*')
          ->where('id', Auth::user()->id)
          ->get();


      $cidade = DB::table('cidades')
          ->select('cidades.id', 'cidades.cidade')
          ->orderBy('cidade')
          ->get();

      return view('estabelecimento/conta/conta', compact('editUsuario','cidade'));
    }

    public function estaUpdate(Request $request, $id){

      $nomeCompleto = $request->nomeCompleto;
      $email = $request->email;
      $facebook = $request->facebook;
      $twitter = $request->twitter;
      $instagram = $request->instagram;
      $cidade = $request->cidade_id;


      $conta = DB::update("update users set nomeCompleto = '$nomeCompleto', email = '$email', facebook = '$facebook', twitter = '$twitter', instagram = '$instagram', cidade_id = $cidade where id = $id");

      return redirect()->route('estaConta');

    }

    public function estaSenhaPag(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      return view('estabelecimento/conta/altSenha');
    }

    public function estaSenha(Request $request){

      $senha = Hash::make($request['senha']);

      $si = Auth::user()->id;

      $senha = DB::update("update users set senha = '$senha' where id = $si");

      return redirect()->route('estaConta');

    }

    public function estaLocal(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $local = DB::table('estabelecimentos')
      ->select('*')
      ->where('usuario_id', Auth::user()->id)
      ->get();

      return view('estabelecimento/conta/altLocal', compact('local'));
    }

    public function updateLocal(Request $request){

        $razaoSocial = $request->razaoSocial;
        $nomeProprietario = $request->nomeProprietario;
        $cnpj = $request->cnpj;
        $logradouro = $request->logradouro;
        $bairro = $request->bairro;

        $si = Auth::user()->id;

        $artista = DB::update("update estabelecimentos set razaoSocial = '$razaoSocial', nomeProprietario = '$nomeProprietario', cnpj = '$cnpj', logradouro = '$logradouro', bairro = '$bairro'  where usuario_id = $si");

        return redirect()->route('editarLocal');
    }

    public function estaUpload(Request $request){
        $foto = $request->file('foto');
        $ext = ['jpg', 'png', 'jpeg'];
        if($foto->isValid() and in_array($foto->extension(), $ext)){
            $foto->storeAs('img/foto', Auth::user()->estabelecimento_id.'-'.Auth::user()->id.'.jpg');
            return redirect()->route('realPublicacao');
        }
    }

    public function logout() {

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      Auth::logout();
      return redirect('/login');
    }
}
