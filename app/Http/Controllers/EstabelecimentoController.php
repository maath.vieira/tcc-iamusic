<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class EstabelecimentoController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function dash(){
      return view('/layouts/estabelecimento');
  }

  public function cadastroEstabelecimento(){
    return view('estabelecimento/cadastroE');
  }

  public function estaEstabe(){

    $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

    if($tipoUsuario){
      return view('cadastro/cadastroBanda');
    }

    $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

    $stdClass = json_decode(json_encode($tipo));

    if($stdClass[0]->tipoUsuario_id === 2){
          return redirect()->route('realPublicacao');
    }

    $estabe = DB::table('users')
        ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
        ->join('estabelecimentos', 'users.estabelecimento_id', '=', 'estabelecimentos.id')
        ->select('estabelecimentos.*','cidades.cidade', 'cidades.uf')
        ->get();

    return view('/estabePerfil/estabelecimentos', compact('estabe'));
  }

  public function perfilEstablecimento($id){

    $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

    if($tipoUsuario){
      return view('cadastro/cadastroBanda');
    }

    $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

    $stdClass = json_decode(json_encode($tipo));

    if($stdClass[0]->tipoUsuario_id === 2){
          return redirect()->route('realPublicacao');
    }

    $perfilEstabe = DB::table('estabelecimentos')
                  ->select('estabelecimentos.*', 'cidades.*', 'users.*')
                  ->join('users', 'estabelecimentos.usuario_id', '=', 'users.id')
                  ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
                  ->where('users.estabelecimento_id', $id)
                  ->get();

    $publiEstabe = DB::table('estabelecimentos')
                  ->select('publicacoes.*', 'estabelecimentos.razaoSocial')
                  ->join('users', 'estabelecimentos.usuario_id', '=', 'users.id')
                  ->join('publicacoes', 'publicacoes.usuario_id', '=', 'users.id')
                  ->where('users.estabelecimento_id', $id)
                  ->where('isAtivo', 0)
                  ->orderByRaw('created_at DESC')
                  ->get();

    $agendaEstabe = DB::table('agendas')
                  ->join('artistas', 'agendas.artista_id', '=', 'artistas.id')
                  ->join('users', 'artistas.usuario_id', '=', 'users.id')
                  ->join('estabelecimentos', 'agendas.estabelecimento_id', '=', 'estabelecimentos.id')
                  ->select('agendas.*', 'artistas.nomeArtistico')
                  ->where('users.estabelecimento_id', $id)
                  ->where('isAtivo', 0)
                  ->limit(5)
                  ->get();

    $comentarios = DB::table('comentarios')
                ->join('estabelecimentos', 'comentarios.estabelecimento_id', 'estabelecimentos.id')
                ->join('artistas', 'comentarios.artista_id', 'artistas.id')
                ->join('aux_comentario', 'comentarios.comentario_id', 'aux_comentario.id')
                ->select('aux_comentario.comentario')
                ->where('isAtivo', 0)
                ->where('flg_enviada', 1)
                ->where('status', 0)
                ->where('comentarios.estabelecimento_id', $id)
                ->limit(5)
                ->get();

                $nota = DB::table('rankinge')
                          ->select(DB::raw('sum(nota)/count(id) as nota'))
                          ->where('artista_id', [Auth::user()->artista_id])
                          ->where('flg_enviado', 1)
                          ->get();

    return view('/perfilE/perfil', compact('perfilEstabe', 'publiEstabe', 'agendaEstabe', 'comentarios', 'nota'));
  }
}
