<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Proposta;
use App\Estabelecimento;
use App\User;

class PropostaController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function verifica2(){
    $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
    $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

    $stdClass = json_decode(json_encode($tipo));

    if($stdClass[0]->tipoUsuario_id === 1){
      if($tipoUsuario){
          return redirect()->route('inicio');
      }
    } elseif($stdClass[0]->tipoUsuario_id === 2){
          return redirect()->route('realPublicacao');
    }
  }

    public function index(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $proposta = DB::table('propostas')
            ->select('propostas.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial')
            ->join('users', 'propostas.artista_id', '=', 'users.artista_id')
            ->join('artistas', 'propostas.artista_id', '=', 'artistas.id')
            ->join('estabelecimentos', 'propostas.estabelecimento_id', '=', 'estabelecimentos.id')
            ->where('propostas.pendente', 0)
            ->where('users.artista_id', Auth::user()->artista_id)
            ->get();

        return view('proposta/proposta', compact('proposta'));
    }

    public function aceitar($id, Request $request){

        $inativar = DB::update("update propostas set pendente = 1, aceita = 0, recusada = 1 where id = ?",
            [$id]);

        $popular = DB::update("update agendas set isAtivo = 0 where id = ?", [$id]);

        return redirect()->route('proposta');
    }

    public function recusada($id){
        $inativar = DB::update("update propostas set pendente = 1, aceita = 1, recusada = 0 where id = ?",
            [$id]);

        return redirect()->route('proposta');
    }

    public function listarAceitas(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $listar = DB::table('propostas')
            ->join('artistas', 'propostas.artista_id', '=', 'artistas.id')
            ->join('estabelecimentos', 'propostas.estabelecimento_id', '=', 'estabelecimentos.id')
            ->select('propostas.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial', 'estabelecimentos.cnpj')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('aceita', 0)
            ->get();

        return view('proposta/propostaAceita', compact('listar'));
    }

    public function listarRecusadas(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $listar = DB::table('propostas')
            ->join('artistas', 'propostas.artista_id', '=', 'artistas.id')
            ->join('estabelecimentos', 'propostas.estabelecimento_id', '=', 'estabelecimentos.id')
            ->select('propostas.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial', 'estabelecimentos.cnpj')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('recusada', 0)
            ->get();

        return view('proposta/propostaRecusada', compact('listar'));
    }


    //proposta estabelecimentos

    public function estaProposta(){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $artistas = DB::table('artistas')
          ->select('*')
          ->get();

      return view('/estabelecimento/proposta/proposta', compact('artistas'));
    }

    public function estaEnviarProposta(Request $request){

      $postagem = DB::insert("insert into propostas (id, dataEvento, valor, artista_id, estabelecimento_id, pendente, aceita, recusada) values (null,?,?,?,?,0,1,1)",
          [$request->dataEvento, $request->valor, $request->artista_id, $request->estabelecimento_id]);

       $inativo = 1;
          $popular = DB::insert("insert into agendas (data, artista_id, estabelecimento_id, isAtivo) VALUES (?,?,?,?)",
          [$request->dataEvento, $request->artista_id, $request->estabelecimento_id, $inativo]);
/**
          $rankingb = DB::insert("insert into rankinge (artista_id, data) VALUES (?,?)",
        [$request->artista_id, $request->dataEvento]);
**/
          return redirect()->route('propostasEnviadas');
    }

    public function estaEnviadas(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $lista = DB::table('propostas')
          ->select('propostas.*', 'artistas.nomeArtistico')
          ->join('users', 'propostas.artista_id', '=', 'users.artista_id')
          ->join('artistas', 'propostas.artista_id', '=', 'artistas.id')
          ->join('estabelecimentos', 'propostas.estabelecimento_id', '=', 'estabelecimentos.id')
          ->get();

      return view('/estabelecimento/proposta/propostasEnviadas', compact('lista'));
    }

    public function estaDelete($id){
        $inativar = DB::delete("delete from propostas where id = ?",
            [$id]);

        return redirect()->route('propostasEnviadas');
    }
}
