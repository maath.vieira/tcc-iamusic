<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Publicacao;
use \Illuminate\Support\Str;

class PublicacaoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verifica(){

        $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
        $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

        $stdClass = json_decode(json_encode($tipo));

        if($stdClass[0]->tipoUsuario_id === 1){
          if($tipoUsuario){
              return view('cadastro/cadastroBanda');
          } else {
              return redirect()->route('inicio');
          }
        } elseif($stdClass[0]->tipoUsuario_id === 2){
              return redirect()->route('realPublicacao');
        }
      }

      public function verifica2(){
        $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
        $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

        $stdClass = json_decode(json_encode($tipo));

        if($stdClass[0]->tipoUsuario_id === 1){
          if($tipoUsuario){
              return redirect()->route('inicio');
          }
        } elseif($stdClass[0]->tipoUsuario_id === 2){
              return redirect()->route('realPublicacao');
        }
      }
/**
        else {
          if($stdClass[0]->tipoUsuario_id === 1){
            echo 'correto';
          } else {
            echo 'eiiiita';
          }
        }

**/

/**
        $tipoUsuario = DB::table('users')
            ->select('artista_id')
            -> where('artista_id', Auth::user()->artista_id)
            ->get();
**/
/**
        if($tipoUsuario){
            return view('cadastro/cadastroBanda');
        } else {
          return redirect()->route('inicio');
        }

        **/

    public function index()
    {

      $usuario = [Auth::user()->artista_id];
      $usu = json_decode(json_encode($usuario));
      $rankverifica = DB::select("select * from rankinge where data < curdate() and respondida = 1 and artista_id = $usu[0]");

      if($rankverifica){
        return redirect()->route('rankingBanda');
      //  return view('ranking/rankingb');
      }

        $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
        $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

        $stdClass = json_decode(json_encode($tipo));

        if($stdClass[0]->tipoUsuario_id === 2){
              return redirect()->route('realPublicacao');
        }

        $postagens = DB::table('publicacoes')
            ->join('users', 'publicacoes.usuario_id', '=', 'users.id')
            ->join('artistas', 'users.artista_id', '=', 'artistas.id')
            ->select('publicacoes.*', 'artistas.nomeArtistico')
            ->where('publicacoes.usuario_id', Auth::user()->id)
            ->where('isAtivo', 0)
            ->orderByRaw('created_at DESC')
            ->get();

        return view('artista/postagem', compact('postagens'));


    }

    public function store(Request $request){

          $cliente = DB::insert("insert into aux_artista_estabele (user, artista_id) values (?,?)",
          [$request->usuario_id, $request->artista_id]);

            $vidoReplace = Str::replaceFirst('watch?v=', 'embed/', $request->video);
            $audioReplace = Str::replaceFirst('https://', 'https%3A//', $request->sound);

            $postagem = DB::insert("insert into publicacoes (descricao, video, sound, usuario_id) values (?,?,?,?)",
                [$request->descricao, $vidoReplace, $audioReplace, $request->usuario_id]);

        return redirect()->route('inicio');

    }

    public function storeEstabe(Request $request){

            $cliente = DB::insert("insert into aux_artista_estabele (user, estabelecimento_id) values (?,?)",
            [$request->usuario_id, $request->estabelecimento_id]);

            $vidoReplace = Str::replaceFirst('watch?v=', 'embed/', $request->video);

            $postagem = DB::insert("insert into publicacoes (descricao, video, usuario_id) values (?,?,?)",
                [$request->descricao, $vidoReplace, $request->usuario_id]);

        return redirect()->route('realPublicacao');

    }

    public function delete($id){
        $inativar = DB::update("update publicacoes set isAtivo = 1 where id = ?",
            [$id]);

        return redirect()->route('inicio');
    }

    public function edit($id){

        $publi = DB::table('publicacoes')
            ->select('publicacoes.*')
            ->where('id', $id)
            ->get();

        return view('artista/editar', compact('publi'));
    }

    public function update(Request $request, $id){

        $publi = DB::update("update publicacoes set descricao = ? where id = $id", [$request->descricao]);

        return redirect()->route('inicio');
    }


    //publicações estabelecimentos

    public function estaPublicar(){

      $usuario = [Auth::user()->estabelecimento_id];
      $usu = json_decode(json_encode($usuario));
      $rankverifica = DB::select("select * from rankinge where data < curdate() and respondida = 1 and estabelecimento_id = $usu[0]");

      if($rankverifica){
        return redirect()->route('rankingp');
      //  return view('ranking/rankingb');
      }

        $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

        if($tipoUsuario){
          return redirect()->route('cadastroEstabelecimento');
        }

        $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

        $stdClass = json_decode(json_encode($tipo));

        if($stdClass[0]->tipoUsuario_id === 1){
              return redirect()->route('inicio');
        }

      $postagem = DB::table('publicacoes')
                ->join('estabelecimentos', 'publicacoes.usuario_id', '=', 'estabelecimentos.usuario_id')
                ->select('publicacoes.*', 'estabelecimentos.razaoSocial')
                ->where('publicacoes.usuario_id', Auth::user()->id)
                ->where('isAtivo', 0)
                ->orderByRaw('created_at DESC')
                ->get();

      return view('/estabelecimento/publicacoes/publicacao', compact('postagem'));
    }

    public function estaDelete($id){
        $inativar = DB::update("update publicacoes set isAtivo = 1 where id = ?",
            [$id]);

        return redirect()->route('realPublicacao');
    }

    public function estaEditar($id){

        $publicacao = DB::table('publicacoes')
            ->select('publicacoes.*')
            ->where('id', $id)
            ->get();

        return view('estabelecimento/publicacoes/editarPublicacao', compact('publicacao'));
    }

    public function estaUpdate(Request $request, $id){

        $publi = DB::update("update publicacoes set descricao = ? where id = $id", [$request->descricao]);

        return redirect()->route('realPublicacao');
    }

}
