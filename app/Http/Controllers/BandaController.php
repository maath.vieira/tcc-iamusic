<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class BandaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verifica(){

        $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
        //$tipoUsuario = DB::select("select artista_id from aux_usuario_artista_estabe where user = ?", Auth::user()->id);
/**
        $tipoUsuario = DB::table('users')
            ->select('artista_id')
            -> where('user', Auth::user()->id)
            ->get();
**/
        if($tipoUsuario){
            return view('cadastro/cadastroBanda');
        } else {
          return redirect()->route('inicio');
        }
    }

    public function index(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $bandas = DB::table('publicacoes')
            ->join('users', 'publicacoes.usuario_id', '=', 'users.id')
            ->join('artistas', 'users.artista_id', '=', 'artistas.id')
            ->select('publicacoes.*', 'users.*', 'artistas.*', 'publicacoes.created_at')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('isAtivo',0)
            ->get();

        return view('banda/banda', compact('bandas'));
    }

//pesquisa banda - estabelecimento

    public function estaBandaPesquisa(Request $request){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $banda = DB::table('users')
          ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
          ->join('artistas', 'users.artista_id', '=', 'artistas.id')
          ->join('agendas', 'users.artista_id', '=', 'agendas.artista_id')
          ->select('artistas.*','cidades.*', 'artistas.*', 'agendas.data')
          ->Where('agendas.data', '!=', $request->dataPesquisa)
          ->Where('agendas.isAtivo', '=', '0')
          ->get();

      return view('/estabelecimento/bandas/dataPesquisada', compact('banda'));

    }

    public function estaBanda(Request $request){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

if($request->dataPesquisa == ''){
        $banda = DB::table('users')
            ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
            ->join('artistas', 'users.artista_id', '=', 'artistas.id')
            ->select('artistas.*','cidades.*', 'artistas.*')
            ->get();
}else{
  $banda = DB::table('users')
      ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
      ->join('artistas', 'users.artista_id', '=', 'artistas.id')
      ->join('agendas', 'users.artista_id', '=', 'agendas.artista_id')
      ->select('artistas.*', 'cidades.*', 'artistas.*')
      ->distinct('artistas.nomeArtistico')
      ->Where('agendas.data', '!=', $request->dataPesquisa)
      ->Where('agendas.isAtivo', '=', '0')
      ->get();
}
      return view('/estabelecimento/bandas/bandas', compact('banda'));
    }

    public function estaBandaPerfil($id){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $perfilBanda = DB::table('artistas')
                    ->select('artistas.*', 'cidades.*', 'users.*')
                    ->join('users', 'artistas.usuario_id', '=', 'users.id')
                    ->join('cidades', 'users.cidade_id', '=', 'cidades.id')
                    ->where('users.artista_id', $id)
                    ->get();

      $publiBanda = DB::table('artistas')
                    ->select('publicacoes.*', 'artistas.nomeArtistico')
                    ->join('users', 'artistas.usuario_id', '=', 'users.id')
                    ->join('publicacoes', 'publicacoes.usuario_id', '=', 'users.id')
                    ->where('users.artista_id', $id)
                    ->where('isAtivo', 0)
                    ->orderByRaw('created_at DESC')
                    ->get();

      $agendaBanda = DB::table('agendas')
                    ->join('artistas', 'agendas.artista_id', '=', 'artistas.id')
                    ->join('users', 'artistas.usuario_id', '=', 'users.id')
                    ->join('estabelecimentos', 'agendas.estabelecimento_id', '=', 'estabelecimentos.id')
                    ->select('agendas.*', 'estabelecimentos.razaoSocial')
                    ->where('users.artista_id', $id)
                    ->where('isAtivo', 0)
                    ->limit(5)
                    ->get();

                    $comentarios = DB::table('comentarios')
                                ->join('estabelecimentos', 'comentarios.estabelecimento_id', 'estabelecimentos.id')
                                ->join('artistas', 'comentarios.artista_id', 'artistas.id')
                                ->join('aux_comentario', 'comentarios.comentario_id', 'aux_comentario.id')
                                ->select('aux_comentario.comentario')
                                ->where('isAtivo', 0)
                                ->where('flg_enviada', 2)
                                ->where('status', 0)
                                ->where('comentarios.artista_id', $id)
                                ->limit(5)
                                ->get();

      return view('/estabelecimento/perfil/perfil', compact('perfilBanda', 'publiBanda', 'agendaBanda', 'comentarios'));
    }

    public function perfilProposta(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $artistas = DB::table('artistas')
          ->select('*')
          ->get();

      return view('/estabelecimento/perfil/EnviarProposta', compact('artistas'));
    }

    public function estabelecimentoPerfil(){


    }
}
