<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('home');

        $artista = DB::table('users')
            ->select('users.artista_id')
            ->get();

       $tipoUsuario = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

          if(is_null($artista) || equalTo($artista, 0)){
              return view('cadastro/cadastroBanda');
          } else {
              return redirect('/');
          }




    }
}
