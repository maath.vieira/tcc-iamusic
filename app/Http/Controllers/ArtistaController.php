<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Artista;
use App\User;
use Illuminate\Support\Facades\Auth;

class ArtistaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //
    }

    public function store(Request $request){
        $artista = DB::insert("insert into artistas (nomeArtistico, cpf, integrantes, usuario_id) values (?,?,?, ?)",
            [$request->nomeArtistico, $request->cpf, $request->integrantes, $request->usuario_id]);

        $idusuario = $request->usuario_id;

        $id = DB::table('artistas')
            ->select('artistas.id')
            ->where('usuario_id', Auth::user()->id)
            ->first()
            ->id;

        $envio = DB::update("update users set artista_id = '$id' where id = '$idusuario'");

        DB::insert("insert into aux_artista_estabele (user, artista_id) values ($idusuario,$id)");

        return redirect()->route('inicio');
    }
}
