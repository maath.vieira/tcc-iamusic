<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Agenda;

class AgendaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verifica2(){
      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
        if($tipoUsuario){
            return redirect()->route('inicio');
        }
      } elseif($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }
    }

    public function index(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }
      
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $agenda = DB::table('agendas')
            ->join('artistas', 'agendas.artista_id', '=', 'artistas.id')
            ->join('estabelecimentos', 'agendas.estabelecimento_id', '=', 'estabelecimentos.id')
            ->select('agendas.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial', 'estabelecimentos.logradouro')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('isAtivo', 0)
            ->get();

        $local = DB::table('estabelecimentos')
            ->select('estabelecimentos.id', 'estabelecimentos.razaoSocial')
            ->get();

        return view('agenda/agenda', compact('agenda', 'local'));
    }

    public function delete($id){
        $inativar = DB::update("update agendas set isAtivo = 1 where id = ?", [$id]);

        return redirect()->route('agenda');
    }

    public function store(Request $request){

        $postagem = DB::insert("insert into agendas (data, artista_id, estabelecimento_id, isAtivo) values (?,?,?,?)",
            [$request->data, $request->artista_id, $request->estabelecimento_id, $request->isAtivo]);

        return redirect()->route('agenda');
    }

    public function edit($id){

        $editar = DB::table('agendas')
            ->select('agendas.*')
            ->where('id', $id)
            ->get();

        $local = DB::table('estabelecimentos')
            ->join('agendas', 'estabelecimentos.id', '=','agendas.estabelecimento_id')
            ->select('estabelecimentos.*')
            ->where('agendas.id', $id)
            ->get();

        return view('agenda/editar', compact('editar', 'local'));
    }

    public function update(Request $request, $id){

        $publi = DB::update("update agendas set data = ? where id = $id", [$request->data]);

        return redirect()->route('agenda');
    }

    //agenda estabelecimentos

    public function estaAgenda(){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $evento = DB::table('agendas')->orderBy('agendas.data')
          ->join('artistas', 'agendas.artista_id', '=', 'artistas.id')
          ->join('estabelecimentos', 'agendas.estabelecimento_id', '=', 'estabelecimentos.id')
          ->join('users', 'estabelecimentos.usuario_id', '=', 'users.id')
          ->select('agendas.*', 'artistas.nomeArtistico')
          ->where('users.id', Auth::user()->id)
          ->where('isAtivo', 0)
          ->get();

          $artista = DB::table('artistas')
              ->select('artistas.id', 'artistas.nomeArtistico')
              ->get();

      return view('/estabelecimento/agenda/agenda', compact('evento', 'artista'));
    }

    public function storeEsta(Request $request){

        $postagem = DB::insert("insert into agendas (data, artista_id, estabelecimento_id, isAtivo) values (?,?,?,?)",
            [$request->data, $request->artista_id, $request->estabelecimento_id, $request->isAtivo]);

        return redirect()->route('realAgenda');
    }

    public function estaDelete($id){
        $inativar = DB::update("update agendas set isAtivo = 1 where id = ?", [$id]);

        return redirect()->route('realAgenda');
    }
}
