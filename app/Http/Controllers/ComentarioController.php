<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $comentario = DB::table('comentarios')
            ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
            ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
            ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
            ->select('comentarios.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial', 'aux_comentario.*')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('isAtivo', 0)
            ->where('flg_enviada', 2)
            ->get();


          $estabe = DB::table('estabelecimentos')
          ->select('*')
          ->get();

        return view('comentario/comentario', compact('comentario', 'estabe'));
    }

    public function arInserir(Request $request){

        $inserir = DB::insert("insert into aux_comentario (comentario, isAtivo, flg_enviada, status) VALUES (?,?,?,?)",
        [$request->comentario, $request->isAtivo, $request->flg_enviada, $request->status]);

        $comentario = DB::table('aux_comentario')
        ->max('id');

        $inserir2 = DB::insert("insert into comentarios (artista_id, estabelecimento_id, comentario_id) VALUES (?,?,?)",
        [$request->artista_id, $request->estabelecimento_id, $comentario]);

        return redirect()->route('comentario');
    }

    public function aceitos(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $comentario = DB::table('comentarios')
        ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
        ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
        ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
        ->select('comentarios.*', 'artistas.*', 'estabelecimentos.*', 'aux_comentario.*')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('isAtivo', 1)
            ->where('flg_enviada', 2)
            ->get();

        return view('comentario/aceitos', compact('comentario'));
    }

    public function feitos(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return view('cadastro/cadastroBanda');
      }

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 2){
            return redirect()->route('realPublicacao');
      }

        $comentario = DB::table('comentarios')
        ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
        ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
        ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
        ->select('comentarios.*', 'artistas.nomeArtistico', 'estabelecimentos.razaoSocial', 'aux_comentario.*')
            ->where('artista_id', Auth::user()->artista_id)
            ->where('isAtivo', 0)
            ->where('flg_enviada', 1)
            ->get();

        return view('comentario/feitos', compact('comentario'));
    }

    public function comentarioAceito($id){
        $aceitar = DB::update("update aux_comentario set isAtivo = 1 where id = ?", [$id]);

        return redirect()->route('comentario');
    }

    public function comentarioRetirar($id){
        $retirar = DB::update("update aux_comentario set isAtivo = 0 where id = ?", [$id]);

        return redirect()->route('comentariosAceitos');
    }

    public function delete($id){
        $aceitar = DB::update("update aux_comentario set isAtivo = -1 where id = ?", [$id]);

        return redirect()->route('comentariosFeitos');
    }

    public function estaComentario(){

      $tipoUsuario = DB::select("select estabelecimento_id from users where id = ? and estabelecimento_id is null", [Auth::user()->id]);

      if($tipoUsuario){
        return redirect()->route('cadastroEstabelecimento');
      }

      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

          $comentario = DB::table('comentarios')
          ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
          ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
          ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
          ->select('comentarios.*', 'artistas.*', 'estabelecimentos.*', 'aux_comentario.*')
              ->where('estabelecimento_id', Auth::user()->estabelecimento_id)
              ->where('isAtivo', 0)
              ->where('flg_enviada', 1)
              ->where('status', 1)
              ->get();

              $artistas = DB::table('artistas')
              ->select('*')
              ->get();

      return view('/estabelecimento/comentario/comentario', compact('comentario', 'artistas'));
    }

    public function estaComentarioAceito(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $comentario = DB::table('comentarios')
      ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
      ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
      ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
      ->select('comentarios.*', 'artistas.*', 'estabelecimentos.*', 'aux_comentario.*')
          ->where('estabelecimento_id', Auth::user()->estabelecimento_id)
          ->where('isAtivo', 0)
          ->where('flg_enviada', 1)
          ->where('status', 0)
          ->get();

      return view('/estabelecimento/comentario/ecomentarioaceito', compact('comentario'));
    }

    public function estaComentarioEnviado(){

      $tipoUsuario = DB::select("select artista_id from users where id = ? and artista_id is null", [Auth::user()->id]);
      $tipo = DB::select("select tipoUsuario_id from users where id = ?", [Auth::user()->id]);

      $stdClass = json_decode(json_encode($tipo));

      if($stdClass[0]->tipoUsuario_id === 1){
            return redirect()->route('inicio');
      }

      $comentario = DB::table('comentarios')
      ->join('aux_comentario', 'comentarios.comentario_id', '=', 'aux_comentario.id')
      ->join('artistas', 'comentarios.artista_id', '=', 'artistas.id')
      ->join('estabelecimentos', 'comentarios.estabelecimento_id', '=', 'estabelecimentos.id')
      ->select('comentarios.*', 'artistas.*', 'estabelecimentos.*', 'aux_comentario.*')
          ->where('estabelecimento_id', Auth::user()->estabelecimento_id)
          ->where('flg_enviada', 2)
          ->whereIn('isAtivo',[0,1])
          ->get();

      return view('/estabelecimento/comentario/ecomentarioenviado', compact('comentario'));
    }

    public function estaInserirComentario(Request $request){

      $inserir = DB::insert("insert into aux_comentario (comentario, flg_enviada, status) VALUES (?,?,?)",
      [$request->comentario, $request->flg_enviada, $request->status]);

      $comentario = DB::table('aux_comentario')
      ->max('id');

      $inserir2 = DB::insert("insert into comentarios (artista_id, estabelecimento_id, comentario_id) VALUES (?,?,?)",
      [$request->artista_id, $request->estabelecimento_id, $comentario]);

      return redirect()->route('comentarioEstabe');
    }

    public function estaDelete($id){
        $aceitar = DB::update("update aux_comentario set isAtivo = -1 where id = ?", [$id]);

        return redirect()->route('comentarioEstabe');
    }

    public function estaAceitar($id){
        $aceitar = DB::update("update aux_comentario set status = 0 where id = ?", [$id]);

        return redirect()->route('comentarioEstabe');
    }

    public function estaRetirar($id){
        $retirar = DB::update("update aux_comentario set status = 1 where id = ?", [$id]);

        return redirect()->route('comentarioEstabe');
    }
}
