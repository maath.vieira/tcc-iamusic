<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Publicacao;
use \Illuminate\Support\Str;

class RankingController extends Controller
{
    public function index(){
      $ranki = DB::table('rankinge')
      ->select('*')
      ->where('data', '<',DB::raw('curdate()'))
      ->where('respondida', 1)
      ->where('artista_id', [Auth::user()->artista_id])
      ->get();

      return view('ranking/rankingb', compact('ranki'));
    }

    public function listaRank(){
      $ranki2 = DB::table('rankinge')
      ->join('artistas', 'rankinge.artista_id', '=', 'artistas.id')
      ->select('rankinge.*', 'artistas.nomeArtistico')
      ->where('respondida', 0)
      ->where('flg_enviado', 1)
      ->orderByRaw('nota DESC')
      ->get();

      $ranki3 = DB::table('rankinge')
      ->join('estabelecimentos', 'rankinge.estabelecimento_id', '=', 'estabelecimentos.id')
      ->select('rankinge.*', 'estabelecimentos.razaoSocial')
      ->where('respondida', 0)
      ->where('flg_enviado', 2)
      ->orderByRaw('nota DESC')
      ->get();

      return view('estabelecimento/rankinge/rankinge', compact('ranki2', 'ranki3'));
    }

    public function inserir(Request $request, $id){
      $nota1 = (float) $request->nota_um;
      $nota2 = (float) $request->nota_dois;
      $nota3 = (float) $request->nota_tres;

        $calc = ($nota1+$nota2+$nota3)/3;

        $insere = DB::update("update rankinge set nota = $calc, respondida = 0 where id = $id");

        return redirect()->route('inicio');
    }

    public function indexDois(){
      $ranki = DB::table('rankinge')
      ->select('*')
      ->where('data', '<',DB::raw('curdate()'))
      ->where('respondida', 1)
      ->where('estabelecimento_id', [Auth::user()->estabelecimento_id])
      ->get();

      return view('/estabelecimento/rankinge/rankingp', compact('ranki'));
    }

    public function listaRankDois(){
      $ranki2 = DB::table('rankinge')
      ->join('artistas', 'rankinge.artista_id', '=', 'artistas.id')
      ->select('rankinge.*', 'artistas.nomeArtistico')
      ->where('respondida', 0)
      ->where('flg_enviado', 1)
      ->orderByRaw('nota DESC')
      ->get();

      $ranki3 = DB::table('rankinge')
      ->join('estabelecimentos', 'rankinge.estabelecimento_id', '=', 'estabelecimentos.id')
      ->select('rankinge.*', 'estabelecimentos.razaoSocial')
      ->where('respondida', 0)
      ->where('flg_enviado', 2)
      ->orderByRaw('nota DESC')
      ->get();

      return view('ranking/rankingv', compact('ranki2', 'ranki3'));
    }

    public function inserirDois(Request $request, $id){
      $nota1 = (float) $request->nota_um;
      $nota2 = (float) $request->nota_dois;
      $nota3 = (float) $request->nota_tres;

        $calc = ($nota1+$nota2+$nota3)/3;

        $insere = DB::update("update rankinge set nota = $calc, respondida = 0 where id = $id");

        return redirect()->route('realPublicacao');
    }

}
