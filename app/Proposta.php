<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    protected $fillable = [
        'dataEvento', 'valor', 'artista_id', 'estabelecimento_id', 'pendente', 'aceita', 'recusada'
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['propostas'];


    public function estabe(){
        return $this->belongsTo('App\Estabelecimento', 'id');
    }
}
