<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    protected $fillable = [
        'nomeArtistico', 'cpf', 'banda', 'integrantes'
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['artistas'];

}
