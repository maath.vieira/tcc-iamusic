<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = [
        'usuario_id', 'foto',
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['fotos'];

}
