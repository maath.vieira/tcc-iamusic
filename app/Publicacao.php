<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicacao extends Model
{
    protected $fillable = [
        'descricao', 'usuario_id'
    ];

    protected  $guarded = [
        'id', 'created_at', 'update_at'
    ];

    protected $table = ['publicacoes'];

    public function usuario(){
        return $this->belongsTo('App\User', 'usuario_id');
    }


}
