<p align="center"><img src="https://uploaddeimagens.com.br/images/001/757/311/original/logo.png?1543785063"></p>

## Sobre iAMusic

O sistema proposto será voltado para músicos, tendo como finalidade facilitar a divulgação e a contratação para apresentações. Este sistema proporcionará acesso a dois tipos de usuários:  artistas (banda, músico, orquestra, ...) e estabelecimentos (empresas, bares, casas de show, ...).
O usuário (artista) poderá adicionar fotos, vídeos, músicas e locais em sua agenda. O usuário (estabelecimento) poderá fazer propostas aos artistas, adicionar bandas à sua agenda, fazer publicações de novidades e adicionar fotos do local.
Desta forma, o público em geral poderá acessar a página dos locais(estabelecimentos) ou da banda(artista) para saber sobre as novidades e verificar agendas.

## Como executar o projeto?

- Baixe e instale o [composer](https://getcomposer.org/).

- Baixe e instale o [xampp](https://www.apachefriends.org/pt_br/index.html).  
Inicialize o Xampp e depois Apache e MySQL.

- Importe a base que está na pasta BD no repositório.

- Entre no diretório em que o sistema está pelo CMD (Prompt de Comando).  
    Coloque o comando <code>php artisan serve</code> e aperte enter.
    
- Entre no navegador da sua escolha e coloque <code>localhost:8000</code>

## Tecnologias utilizadas

Abaixo irei apresentar quais tecnologias utilizadas e a sua documentação: 

- [Php](http://php.net/docs.php)
- [Laravel](https://laravel.com/docs/5.7)
- [MySQL](https://dev.mysql.com/doc/)
- [Android](https://developer.android.com/docs/)
- [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/)
- [API-Paypal](https://developer.paypal.com/)
- [API-Facebook](https://developers.facebook.com/docs/facebook-login/)

## Aluno

Matheus Delgado Vieira (maath.vieeira@gmail.com)

## Professor Orientador

Prof. Me. Gladimir Ceroni Catarino
