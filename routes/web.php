<?php

Auth::routes();

Route::get('/home', function () {
    return redirect('/');
});

//facebook
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

//nome artistico
Route::get('layouts/artista', 'ArtistaController@nome')->name('nomeArtista');

//postagens
Route::get('/', 'PublicacaoController@verifica')->name('verifica');
Route::get('/inicio', 'PublicacaoController@index')->name('inicio');
Route::get('postagens/excluir/{id}', 'PublicacaoController@delete')->name('excluirPost');
Route::post('postagens/inserir', 'PublicacaoController@store')->name('salvarPost');
Route::get('postagens/editar/{id}', 'PublicacaoController@edit')->name('editarPostagem');
Route::put('postagens/atualizar/{id}', 'PublicacaoController@update')->name('atualizarPostagem');

//cadastro do artista
Route::post('cadastroBanda/inserir', 'ArtistaController@store')->name('salvarCadastroBanda');

//proposta
Route::get('/proposta', 'PropostaController@index')->name('proposta');
Route::get('/aceitas', 'PropostaController@listarAceitas')->name('propostaAceita');
Route::get('/recusadas', 'PropostaController@listarRecusadas')->name('propostaRecusada');
Route::get('proposta/aceitar/{id}', 'PropostaController@aceitar')->name('aceitarProposta');
Route::get('proposta/recusar/{id}', 'PropostaController@recusada')->name('recusarProposta');

//agenda
Route::get('/agenda', 'AgendaController@index')->name('agenda');
Route::post('agenda/inserir', 'AgendaController@store')->name('salvarEvento');
Route::get('agenda/excluir/{id}', 'AgendaController@delete')->name('excluirEvento');
Route::get('agenda/editar/{id}', 'AgendaController@edit')->name('editarEvento');
Route::put('agenda/atualizar/{id}', 'AgendaController@update')->name('atualizarEvento');

//comentarios
Route::get('/comentario', 'ComentarioController@index')->name('comentario');
Route::get('/aceitos', 'ComentarioController@aceitos')->name('comentariosAceitos');
Route::get('/feitos', 'ComentarioController@feitos')->name('comentariosFeitos');
Route::get('comentario/aceitar/{id}', 'ComentarioController@comentarioAceito')->name('aceitarComentario');
Route::get('comentario/retirar/{id}', 'ComentarioController@comentarioRetirar')->name('retirarComentario');
Route::get('comentario/excluir/{id}', 'ComentarioController@delete')->name('deletarComentario');
Route::post('comentario/inserirCom', 'ComentarioController@arInserir')->name('inserirCom');

//conta
Route::get('/conta', 'ContaController@index')->name('conta');
Route::get('conta/editar/{id}', 'ContaController@edit')->name('editConta');
Route::put('conta/atualizar/{id}', 'ContaController@update')->name('atualizaConta');
Route::get('conta/editSenha/{id}', 'ContaController@editSenha')->name('editSenha');
Route::put('conta/atualizarSenha/{id}', 'ContaController@updateSenha')->name('atualizaSenha');

//conta/artista
Route::get('/contaArtistica', 'ContaController@indexArtista')->name('contaArtistica');
Route::get('conta/editarArtista/{id}', 'ContaController@editArtista')->name('editArtista');
Route::put('conta/atualizarArtista/{id}', 'ContaController@updateArtista')->name('atualizaArtista');

//imagens
Route::get('/alterarFoto', 'ContaController@alterarFoto')->name('alterarFoto');
Route::post('/upload', 'ContaController@upload')->name('upload');

//banda
Route::get('/banda', 'BandaController@index')->name('banda');

//perfil Estabelecimento
Route::post('/{id}', 'BandaController@estaBandaPerfil')->name('perfilB');
Route::get('/perfilEstablecimento', 'BandaController@estabelecimentoPerfil')->name('perfilEstablecimento');

//perfil Banda
Route::post('/{id}', 'BandaController@estaBandaPerfil')->name('perfilB');
//Route::post('/dataP', 'BandaController@estaBandaPesquisa')->name('dataPesquisada');
Route::get('/perfilBanda', 'BandaController@estaBanda')->name('perfilBanda');
Route::get('/perfilProposta', 'BandaController@perfilProposta')->name('perfilProposta');

//estabelecimento
Route::get('/estabelecimento', 'EstabelecimentoController@dash')->name('estabelecimento');

//proposta
Route::get('/realizarProposta', 'PropostaController@estaProposta')->name('realizarProposta');
Route::get('/propostasEnviadas', 'PropostaController@estaEnviadas')->name('propostasEnviadas');
Route::post('proposta/enviarPropostas', 'PropostaController@estaEnviarProposta')->name('enviarProposta');
Route::get('proposta/excluir/{id}', 'PropostaController@estaDelete')->name('excluirProposta');

//publicação
Route::get('/realPublicacao', 'PublicacaoController@estaPublicar')->name('realPublicacao');
Route::post('postagem/publicar', 'PublicacaoController@storeEstabe')->name('salvarPublicacao');
Route::get('postagem/excluir/{id}', 'PublicacaoController@estaDelete')->name('excluirPosta');
Route::get('postagem/{id}', 'PublicacaoController@estaEditar')->name('editarPosta');
Route::put('postagem/atualizar/{id}', 'PublicacaoController@estaUpdate')->name('atualizarEstaPostagem');

//agenda
Route::get('/realAgenda', 'AgendaController@estaAgenda')->name('realAgenda');
Route::post('agenda/inserirEsta', 'AgendaController@storeEsta')->name('salvarEsta');
Route::get('agenda/eexcluir/{id}', 'AgendaController@estaDelete')->name('exAgenda');

//comentario Estabelecimento
Route::get('/comentarioEstabelecimento', 'ComentarioController@estaComentario')->name('comentarioEstabe');
Route::get('/ecomentarioAceitos', 'ComentarioController@estaComentarioAceito')->name('ecomentarioAceitos');
Route::get('/ecomentarioEnviados', 'ComentarioController@estaComentarioEnviado')->name('ecomentarioEnviados');
Route::post('estabelecimento/inserirComentario', 'ComentarioController@estaInserirComentario')->name('inserirComentario');
Route::get('estabelecimento/comentario/excluir/{id}', 'ComentarioController@estaDelete')->name('exComentario');
Route::get('estabelecimento/comentario/aceitar/{id}', 'ComentarioController@estaAceitar')->name('acComentario');
Route::get('estabelecimento/comentario/retirar/{id}', 'ComentarioController@estaRetirar')->name('reComentario');

//configurações da conta
Route::get('/estabelecimentoConta', 'ContaController@estaConta')->name('estaConta');
Route::put('conta/atualizar/{id}', 'ContaController@estaUpdate')->name('atualizaEstaConta');
Route::get('/editarSenha', 'ContaController@estaSenhaPag')->name('editarSenha');
Route::put('/atualizarSenha', 'ContaController@estaSenha')->name('atualizaEstaSenha');
Route::get('/editarLocal', 'ContaController@estaLocal')->name('editarLocal');
Route::put('/atualizarLocal', 'ContaController@updateLocal')->name('atualizaEstaLocal');
Route::get('/logoutE', 'ContaController@logout')->name('logoutE');

//postagem imagens
Route::post('/upload', 'ContaController@estaUpload')->name('estaUpload');

//cadastro estabelecimento
Route::get('/cadastroEstabelecimento', 'EstabelecimentoController@cadastroEstabelecimento')->name('cadastroEstabelecimento');

//ranking Banda
Route::get('/rankingb', 'RankingController@index')->name('rankingBanda');
Route::post('/insereRanking/{id}', 'RankingController@inserir')->name('insereRanking');

//ranking Esta
Route::get('/rankingp', 'RankingController@indexDois')->name('rankingp');
Route::get('/rankingv', 'RankingController@listaRankDois')->name('rankingv');
Route::post('/insereRankingE/{id}', 'RankingController@inserirDois')->name('insereRankingE');

//ranking estabelecimento
Route::get('/rankinge', 'RankingController@listaRank')->name('rankingEst');

//perfil Estabelecimento
Route::post('a/{id}', 'EstabelecimentoController@perfilEstablecimento')->name('perfilE');
Route::get('/perfilEstabelecimento', 'EstabelecimentoController@estaEstabe')->name('perfilEstabelecimento');

//publicação de fotos
Route::post('/uploadFotoEsta', 'ContaController@publicaFotoEsta')->name('uploadEsta');
